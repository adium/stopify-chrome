### stopify-chrome
================

Allows Spotify users to bypass the 'Spotify Web Player' in Chrome and direct all links on the web to the official desktop app.

Available on the Chrome store @ https://chrome.google.com/webstore/detail/stopify/fhncainmahcgdcoejihgbkfopcmoghlc?hl=en-US